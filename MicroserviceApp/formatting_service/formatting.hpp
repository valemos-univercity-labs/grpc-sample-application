#ifndef __FORMATTING_H__
#define __FORMATTING_H__

#include <string>
#include "formatting.pb.h"
#include "formatting.grpc.pb.h"


class FormattingImpl : public Formatting::Service
{
    virtual ::grpc::Status FormatAddSemicolons(
        ::grpc::ServerContext* context, 
        const ::FormattingRequest* request, 
        ::FormattingResult* response);

    virtual ::grpc::Status FormatCSVTable(
        ::grpc::ServerContext* context, 
        const ::FormattingRequest* request, 
        ::FormattingResult* response);
};

#endif // __FORMATTING_H__