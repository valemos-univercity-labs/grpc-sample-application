#include "formatting.hpp"


::grpc::Status FormattingImpl::FormatAddSemicolons(
    ::grpc::ServerContext* context, 
    const ::FormattingRequest* request, 
    ::FormattingResult* response)
{
    std::string result(request->content());
    for (size_t i = 0; i < result.size(); i++)
    {
        if (result[i] == ' ')
        {
            result.at(i) = ';';
        }
    }
    response->set_id(request->id());
    response->set_result(result);
    return ::grpc::Status::OK;
}

::grpc::Status FormattingImpl::FormatCSVTable(
    ::grpc::ServerContext* context, 
    const ::FormattingRequest* request, 
    ::FormattingResult* response)
{
    std::string result(request->content());
    for (size_t i = 0; i < result.size(); i++)
    {
        if (result[i] == ' ')
        {
            result.at(i) = '\t';
        }
    }
    response->set_id(request->id());
    response->set_result(result);
    return ::grpc::Status::OK;
}