#include <string>
#include <cstdlib>
#include <grpc++/server_builder.h>
#include "formatting.hpp"


int main()
{
    auto* value = std::getenv("SERVICE_PORT");
    std::string service_port{value != nullptr ? value : "50000"};
    
    FormattingImpl service{};

    grpc::ServerBuilder builder;
    builder.AddListeningPort("[::]:" + service_port, grpc::InsecureServerCredentials());
    builder.RegisterService(&service);
    std::unique_ptr<grpc::Server> server(builder.BuildAndStart());
    std::cout << "Server listening on " << service_port << std::endl;
    server->Wait();
}