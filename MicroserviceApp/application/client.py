from __future__ import print_function

import logging
import os
import random
import time

import grpc
import arithmetics_pb2_grpc
from arithmetics_pb2 import OperationType, OperationRequest, OperationResult
import formatting_pb2_grpc
from formatting_pb2 import FormattingRequest, FormattingResult


def run():
    first, second = random.randint(0, 100), random.randint(0, 100)
    
    arithmetics_address = os.environ["ARITHMETICS_URL"]
    with grpc.insecure_channel(arithmetics_address) as channel:
        stub = arithmetics_pb2_grpc.ArithmeticsStub(channel)
        request = OperationRequest(id=1, type=OperationType.ADD, first=first, second=second)
        response = stub.Calculate(request)
        result = response.result
        
    print(f"Operation result: {first} + {second} = {result}")
    
    formatting_address = os.environ["FORMATTING_URL"]
    with grpc.insecure_channel(formatting_address) as channel:
        stub = formatting_pb2_grpc.FormattingStub(channel)
        request = FormattingRequest(id=1, content=" ".join(map(str, [first, second, result])))
        response = stub.FormatAddSemicolons(request)
        print(response.result)

        request = FormattingRequest(id=1, content=" ".join(map(str, [first, second, result])))
        response = stub.FormatCSVTable(request)
        print(response.result)

        
    

if __name__ == '__main__':
    logging.basicConfig()
    
    while True:
        run()
        time.sleep(1)
