from concurrent import futures
import logging
import os

import grpc
import arithmetics_pb2_grpc
from arithmetics_pb2 import OperationResult, OperationType


class Arithmetics(arithmetics_pb2_grpc.ArithmeticsServicer):

    def Calculate(self, request, context):
        first, second = request.first, request.second
        
        operation = request.type
        if OperationType.ADD == operation:
            result = first + second
        else:
            result = first
            
        return OperationResult(id=request.id, result=result)


def serve():
    port = os.environ["SERVICE_PORT"]
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    arithmetics_pb2_grpc.add_ArithmeticsServicer_to_server(Arithmetics(), server)
    server.add_insecure_port('[::]:' + port)
    server.start()
    print("Server started, listening on " + port)
    server.wait_for_termination()


if __name__ == '__main__':
    logging.basicConfig()
    serve()
