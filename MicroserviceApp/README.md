### Docker build commands


```
docker image build -t python_qrpc_microservice_base:latest -f Dockerfile.python_base .
docker image build -t arithmetics_server:latest -f Dockerfile.arithmetics .
docker image build -t formatting_service_server:latest -f Dockerfile.formatting_service .
docker image build -t microservice_client:latest -f Dockerfile.client .
```

### Compile python protobuf

```
python -m grpc_tools.protoc -I ./protobufs --python_out=./application --grpc_python_out=./application ./protobufs/arithmetics.proto
python -m grpc_tools.protoc -I ./protobufs --python_out=./application --grpc_python_out=./application ./protobufs/formatting.proto
```
